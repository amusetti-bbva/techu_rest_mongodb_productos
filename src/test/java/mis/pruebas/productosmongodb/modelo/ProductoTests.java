package mis.pruebas.productosmongodb.modelo;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

public class ProductoTests {

    Producto p;

    @BeforeAll
    public static void antesQueTodo() {
        System.out.println("antesQueTodo");
    }
    @AfterAll
    public static void despuesQueTodo() {
        System.out.println("despuesQueTodo");
    }

    @BeforeEach
    public void crearProducto() {
        System.out.println("@Before: crearProducto()");
        this.p = new Producto();
        System.out.println("===INICIO DEL TEST===");
    }

    @AfterEach
    public void finalizar() {
        System.out.println("===FIN DEL TEST===");
    }

    @Test
    public void testListaInicialProveedores() {
        System.out.println("@Test: testListaInicialProveedores()");
        final Producto p = new Producto();
        assertNotNull(p.getProveedores());
        assertTrue(p.getProveedores().isEmpty());
    }

    @Test
    public void testPrecioInicial() {
        System.out.println("@Test: testPrecioInicial()");
        assertEquals(0.0f, this.p.getPrecio(), "El precio inicial no es 0.0");
    }

}
